import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeedComponent } from './feed/feed.component';
// import { RouteGuardService } from './route-guard.service';
import { PostComponent } from './post/post.component';
import { CategoriesComponent } from './categories/categories.component';
import { CatComponent } from './cat/cat.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactsComponent } from './contacts/contacts.component';


const appRoutes: Routes = [
  { path: '', component: FeedComponent, pathMatch: 'full' },
  { path: 'post/:post', component: PostComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'cat/:cat', component: CatComponent },
  { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule' },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
