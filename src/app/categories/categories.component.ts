import { Component, OnInit, HostBinding } from '@angular/core';
import { home } from '../animations';
import { SharedService } from '../shared.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  animations: [ home ]
})
export class CategoriesComponent implements OnInit {

  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@home') animation = true;
  constructor(
    public shared: SharedService,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle(this.shared.language == 'en' ? "Categiries" : "Категории")
  }

}
