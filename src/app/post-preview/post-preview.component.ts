import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.scss']
})
export class PostPreviewComponent implements OnInit {

  constructor(
    public shared: SharedService
  ) { }

  ngOnInit() {
    let type;
    switch(this.type) {
      case 1: type = 'one';
        break;
      case 2: type = 'two';
        break;
      case 3: type = 'three';
        break;
      case 4: type = 'four';
        break;
      case 6: type = 'six';
        break;
      case 'master': type = 'master';
        break;
      case 'slave': type = 'slave';
        break;
    }
    this.postElement.nativeElement.classList.add(type);
  }
  @Input('post') post;
  @Input('type') type;
  @ViewChild('postElement') postElement: ElementRef;
}
