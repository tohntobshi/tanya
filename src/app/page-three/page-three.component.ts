import { Component, OnInit, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-page-three',
  templateUrl: './page-three.component.html',
  styleUrls: ['./page-three.component.scss']
})
export class PageThreeComponent implements OnInit {


  @Input('page') page;
    constructor(
      public shared: SharedService
    ) { }

  ngOnInit() {
    this.viewWidth = window.innerWidth;
  }
  viewWidth;
  @ViewChild('container') container: ElementRef;
  @ViewChild('image') image: ElementRef;
  @HostListener('window: resize')
  onResize() {
    this.viewWidth = window.innerWidth;
    if (this.viewWidth < 768) {
      return
    }
    try {
      this.image.nativeElement.style.width = '100%';
      this.image.nativeElement.style.left = '0px';
      this.onLoad();
    } catch(err) {
      console.log(err)
    }
  }
  @HostListener('window: scroll')
  onScroll() {
    if (this.viewWidth < 768) {
      return
    }
    let containerposition = this.container.nativeElement.getBoundingClientRect();
    if(containerposition.top > window.innerHeight || containerposition.bottom < 0) {
      return;
    }
    // this.image.nativeElement.style.bottom = -(window.innerHeight - containerposition.bottom)/3 + 'px';
    this.image.nativeElement.style.transform = `translateY(${(window.innerHeight - containerposition.bottom)/3}px)`;
  }
  onLoad() {
    if (this.viewWidth < 768) {
      return
    }
    if (this.image.nativeElement.clientHeight/window.innerHeight < 2/3) {
      let aspectratio = this.image.nativeElement.clientWidth/this.image.nativeElement.clientHeight;
      let width = window.innerHeight * 2/3 * aspectratio;
      this.image.nativeElement.style.width =  width + 'px';
      this.image.nativeElement.style.left = -(width - window.innerWidth)/2 + 'px';
    }
  }
}
