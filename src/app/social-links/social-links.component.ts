import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-social-links',
  templateUrl: './social-links.component.html',
  styleUrls: ['./social-links.component.scss']
})
export class SocialLinksComponent {

  @HostBinding('style.display') display = 'block';
  @HostBinding('style.textAlign') textAlign = 'center';
}
