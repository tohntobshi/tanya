import { Component, OnInit, ViewChild, ElementRef, HostListener, HostBinding, OnDestroy } from '@angular/core';
import { SharedService } from '../shared.service';
import { ActivatedRoute,  } from '@angular/router';
import { post } from '../animations';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  animations: [ post ]
})
export class PostComponent implements OnInit, OnDestroy {
  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@post') animation = true;
  constructor(
    public shared: SharedService,
    private route: ActivatedRoute,
    private title: Title
  ) { }

  ready = false;
  @ViewChild('line') line: ElementRef;
  ngOnInit() {
    this.post_id = this.route.snapshot.paramMap.get('post');
    if (this.post_id) {
      this.shared.fetchPost(this.post_id).then(r => {
        this.title_en = r[0]['title_en'];
        this.title_ru = r[0]['title_ru'];
        this.desc_en = r[0]['desc_en'];
        this.desc_ru = r[0]['desc_ru'];
        this.tags = r[0]['tags'];
        this.images = r[1];
        this.backstage = this.images.reduce((bs, i) => i.position == 'backstage' || bs, false);
        this.ready = true;
        if(!this.destroyed) {
          this.title.setTitle(this.shared.language == 'en' ? this.title_en : this.title_ru);
        }
      })
    }
  }
  backstage = false;
  post_id: string;
  title_en: string;
  title_ru: string;
  desc_en: string;
  desc_ru: string;
  tags = [];
  images: any = [];
  destroyed = false;
  ngOnDestroy() {
    this.destroyed = true;
  }
  /* managing backstage movement */
  @HostListener('window: resize')
  lineLoad() {
    if(!this.backstage) {
      return;
    }
    this.linewidth = Array.from(this.line.nativeElement.children).reduce((sum, el) => sum + el['clientWidth'], 0);
    this.line.nativeElement.style.width = this.linewidth + 500 + 'px';
    this.minoffset = window.innerWidth - this.linewidth;
    this.lineoffset = this.minoffset/2;
    this.applyTranslate();
  }
  minoffset;
  linewidth;
  lineoffset = 0;
  cursorprevpos;
  prevtime;
  speed;
  endtime;
  targetoffset;
  amplitude;
  dragged = false;
  lineMoveStart(e) {
    e.preventDefault();
    e.stopPropagation();
    if (this.minoffset >= 0) {
      return;
    }
    this.cursorprevpos = this.xPos(e);
    this.prevtime = Date.now();
    this.speed = 0;
    this.dragged = true;
    // this.changePosition();
  }
  lineMove(e) {
    e.preventDefault();
    e.stopPropagation();
    if (!this.dragged) {
      return;
    }
    let cursor = this.xPos(e);
    let time = Date.now();
    let cursormove = cursor - this.cursorprevpos;
    let timepassed = time - this.prevtime;

    this.prevtime = time;
    this.cursorprevpos = cursor;

    let v = 1000*cursormove/(timepassed + 1);
    this.speed = 0.8*v + 0.2*this.speed;

    this.applyOffset(this.lineoffset + cursormove);
    this.applyTranslate();
  }

  lineMoveEnd(e) {
    e.preventDefault();
    e.stopPropagation();
    if(!this.dragged) {
      return;
    }
    this.dragged = false;
    this.amplitude = 0.8 * this.speed;
    this.targetoffset = Math.round(this.lineoffset + this.amplitude);
    this.endtime = Date.now();
    requestAnimationFrame(this.autoScroll);
  }
  autoScroll = () => {
    if (this.dragged) {
      return;
    }
    let elapsed, delta;
    if (this.amplitude) {
      elapsed = Date.now() - this.endtime;
      delta = -this.amplitude * Math.exp(-elapsed / 325);
      if (delta > 0.5 || delta < -0.5) {
        this.applyOffset(this.targetoffset + delta);
        this.applyTranslate();
        requestAnimationFrame(this.autoScroll)
      } else {
        this.applyOffset(this.targetoffset);
        this.applyTranslate();
      }
    }
  }
  xPos(e) {
    if (e instanceof TouchEvent) {
      return e.touches[0].clientX;
    } else {
      return e.clientX;
    }
  }
  applyOffset(newoffset) {
    this.lineoffset = newoffset >= 0 ? 0 : newoffset <= this.minoffset ? this.minoffset : newoffset;
  }
  // changePosition() {
  //   if (!this.dragged) {
  //     return;
  //   }
  //   this.applyTranslate();
  //   requestAnimationFrame(this.changePosition.bind(this));
  // }
  applyTranslate() {
    this.line.nativeElement.style.transform = `translateX(${this.lineoffset}px)`;
  }
}
