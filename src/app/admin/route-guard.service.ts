import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SharedService } from '../shared.service';


@Injectable()
export class RouteGuardService implements CanActivate {

  constructor(
    private shared: SharedService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if ((state.url === '/admin/page-manager' || state.url === '/admin/tags' || state.url.startsWith('/admin/post-editor')) && this.shared.admin === false && this.shared.user_cheked === true) {
      this.router.navigate(['/']);
      return false;
    } else if ((state.url === '/admin/page-manager' || state.url === '/admin/tags' || state.url.startsWith('/admin/post-editor')) && this.shared.admin === false && this.shared.user_cheked === false) {
      return new Promise<boolean>(resolve => {
        setTimeout(() => {
          resolve(this.canActivate(route, state));
        }, 100);
      });
    } else {
      return true;
    }
  }


}
