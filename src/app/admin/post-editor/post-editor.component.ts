import { Component, OnInit, HostBinding } from '@angular/core';
import { SharedService } from '../../shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { post } from '../../animations';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post-editor',
  templateUrl: './post-editor.component.html',
  styleUrls: ['./post-editor.component.scss'],
  animations: [ post ]
})
export class PostEditorComponent implements OnInit {
  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@post') animation = true;
  constructor(
    private shared: SharedService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title
  ) { }
  loading = true;
  ngOnInit() {
    this.title.setTitle("Post editor");
    this.post_id = this.route.snapshot.paramMap.get('post');
    if (this.post_id) {
      this.shared.fetchPost(this.post_id).then(r => {
        this.title_en = r[0]['title_en'];
        this.title_ru = r[0]['title_ru'];
        this.short_desc_en = r[0]['short_desc_en'];
        this.short_desc_ru = r[0]['short_desc_ru'];
        this.desc_en = r[0]['desc_en'];
        this.desc_ru = r[0]['desc_ru'];
        this.preview = r[0]['preview'];
        this.content = r[0]['content'];
        this.tags = r[0]['tags'].toString();
        this.images = r[1];
        this.loading = false;
      })
    } else {
      this.loading = false;
    }
  }
  title_en = '';
  title_ru = '';
  short_desc_en = '';
  short_desc_ru = '';
  desc_en = '';
  desc_ru = '';
  post_id: string;
  tags: string = '';
  preview = {};
  images: any = [];
  content = true;
  new_post_id: string;

  addImage(e) {
    this.images.push({
      desc_en: "",
      desc_ru: "",
      position: "left",
      src: e.src,
      type: e.type
    })
  }

  changePreview(e) {
    this.preview = null;
    setTimeout(() => {
      this.preview = e;
    }, 200);
  }

  imageUp(n) {
    if (n == 0) {
      return;
    }
    let tmp = this.images[n-1];
    this.images[n-1] = this.images[n];
    this.images[n] = tmp;
  }
  imageDown(n) {
    if (n == this.images.length - 1) {
      return;
    }
    let tmp = this.images[n+1];
    this.images[n+1] = this.images[n];
    this.images[n] = tmp;
  }
  imageDelete(n) {
    this.images.splice(n, 1);
  }

  submit() {
    this.loading = true;
    let tags_arr = this.tags.trim().split(',').filter(x => x).map(x => x.trim().toLowerCase());
    let tags = {};
    tags_arr.forEach(t => {
      tags[t] = true;
    });
    let post = {
      title_en: this.title_en,
      title_ru: this.title_ru,
      short_desc_en: this.short_desc_en,
      short_desc_ru: this.short_desc_ru,
      desc_en: this.desc_en,
      desc_ru: this.desc_ru,
      preview: this.preview,
      content: this.content,
      tags: tags
    }
    let images = [];
    this.images.forEach((i, index) => {
      images.push({
        desc_en: i.desc_en,
        desc_ru: i.desc_ru,
        position: i.position,
        src: i.src,
        type: i.type,
        order: index
      })
    })
    if(this.new_post_id) {
      this.new_post_id = this.new_post_id.toLowerCase().trim();
    }
    this.shared.submitPost(this.post_id, post, images, this.new_post_id).then(() => {
      this.shared.fetchPages();
      this.router.navigate(['admin/page-manager']);
    })
  }


}
