import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private shared: SharedService,
    private router: Router,
    private title: Title
  ) { }
  username: string = '';
  password: string = '';
  error: string;
  ngOnInit() {
    this.title.setTitle("Login");
  }
  onSubmit() {
    this.shared.logIn(this.username, this.password)
      .then(() => {
        this.router.navigate(['']);
      })
      .catch(e => {
        this.error = e.message;
        setTimeout(() => {
          this.error = null;
        }, 3000);
      })
  }
}
