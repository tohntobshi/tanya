import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from './login/login.component';
import { PageManagerComponent } from './page-manager/page-manager.component';
import { PostEditorComponent } from './post-editor/post-editor.component';
import { AddImageComponent } from './add-image/add-image.component';
import { TagsComponent } from './tags/tags.component';

import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule } from '@angular/forms';

import { RouteGuardService } from './route-guard.service';


@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    LoginComponent,
    PageManagerComponent,
    PostEditorComponent,
    AddImageComponent,
    TagsComponent
  ],
  providers: [ RouteGuardService ]
})
export class AdminModule { }
