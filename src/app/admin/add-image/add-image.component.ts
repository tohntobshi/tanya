import { Component, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../shared.service';
import ImageCompressor from 'image-compressor.js';

@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.component.html',
  styleUrls: ['./add-image.component.scss']
})
export class AddImageComponent {
  @Output('upload') upload = new EventEmitter();

  constructor(
    private shared: SharedService
  ) { }

  compress = true;
  processing = false;
  onUpload(t) {
    if (!t.files[0]) {
      return;
    }
    this.processing = true;
    if (this.compress) {
      new ImageCompressor(t.files[0], {
        quality: .6,
        maxWidth: 2000,
        maxHeight: 2000,
        success: result => {
          this.shared.uploadPicture(result).then(this.onUploadCB.bind(this));
        },
        error: e => {
          this.processing = false;
          alert(e.message);
        }
      })
    } else {
      this.shared.uploadPicture(t.files[0]).then(this.onUploadCB.bind(this));
    }
  }

  onUploadCB(res) {
    this.processing = false;
    if(!res) {
      return;
    }
    this.upload.emit({
      type: 'image',
      src: res
    });
  }

  embedOpen = false;
  embed_link: string;
  onEmbed() {
    if (!this.embed_link) {
      this.embedOpen = false;
      return;
    }
    let raw_link = this.embed_link;
    this.embed_link = '';
    this.embedOpen = false;
    let link = raw_link.split('/');
    if (!link[2]) {
      return;
    }
    let src;
    let type;
    let embed_src;
    if (link[2].slice(-11) === 'youtube.com') {
      let details = link[3].match(/v=[a-zA-Z0-9_\-]+/);
      if(details) {
        src = details[0].slice(2);
        type = 'youtube';
      }
    }
    if (link[2] === 'vimeo.com') {
      let check = /^[0-9]*$/;
      if (check.test(link[link.length - 1])) {
        src = link[link.length - 1];
        type = 'vimeo';
      }
    }
    if (src && type) {
      this.upload.emit({
        src: src,
        type: type
      });
    }
  }
}
