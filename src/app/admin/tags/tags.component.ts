import { Component, OnInit, HostBinding } from '@angular/core';
import { SharedService } from '../../shared.service';
import { home } from '../../animations';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  animations: [home]
})
export class TagsComponent implements OnInit {
  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@home') animation = true;
  constructor(
    private title: Title,
    public shared: SharedService
  ) { }

  ngOnInit() {
    this.title.setTitle("Tag descriptions");
    this.shared.fetchTagDescs();
  }
  tag = "";
  desc_en = "";
  desc_ru = "";
  clearFields() {
    this.tag = "";
    this.desc_en = "";
    this.desc_ru = "";
  }
  exposeTag(tag) {
    this.tag = tag.tag;
    this.desc_en = tag.desc_en;
    this.desc_ru = tag.desc_ru;
  }
  submit() {
    let tag = this.tag.trim().toLowerCase();
    if(!tag) {
      return;
    }
    this.shared.setTag({
      tag: tag,
      desc_en: this.desc_en,
      desc_ru: this.desc_ru
    })
    this.clearFields()
  }
  delete() {
    if(!this.tag) {
      return;
    }
    this.shared.deleteTag(this.tag);
    this.clearFields();
  }
}
