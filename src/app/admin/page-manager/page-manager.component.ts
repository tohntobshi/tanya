import { Component, OnInit, HostListener, HostBinding } from '@angular/core';
import { SharedService } from '../../shared.service';
import { home } from '../../animations';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-manager',
  templateUrl: './page-manager.component.html',
  styleUrls: ['./page-manager.component.scss'],
  animations: [ home ]
})
export class PageManagerComponent implements OnInit {
  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@home') animation = true;
  constructor(
    public shared: SharedService,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle("Page manager")
  }
  postDrag(e, post) {
    e.stopPropagation();
    e.dataTransfer.dropEffect = "move";
    e.dataTransfer.setData('type', 'post');
    e.dataTransfer.setData('id', post.id);
    e.dataTransfer.setData('page', post.page);
  }
  pageDrag(e, page) {
    e.dataTransfer.dropEffect = "move";
    e.dataTransfer.setData('type', 'page');
    e.dataTransfer.setData('id', page.page);
  }
  postDrop(e, post) {
    e.stopPropagation();
    let type = e.dataTransfer.getData('type');
    let id = e.dataTransfer.getData('id');
    let source_page = e.dataTransfer.getData('page');
    if (type == 'page') {
      console.log(`Page ${id} dragged to page ${post.page}`);
      this.shared.switchPages(id, post.page); // pages numbers
      return;
    }
    if (source_page != post.page) {
      console.log(`Post ${id} dragged to page ${post.page}`);
      this.shared.postToPage(id, post.page); // post id, target_page, source_page
    } else {
      console.log(`Post ${id} dragged to post ${post.id}`);
      this.shared.switchPosts(id, post.id, source_page); // two posts, page
    }
  }
  pageDrop(e, page) {
    let type = e.dataTransfer.getData('type');
    let id = e.dataTransfer.getData('id');
    let source_page = e.dataTransfer.getData('page');
    if (type == 'post') {
      this.shared.postToPage(id, page.page); // post id, target_page, source_page
    } else {
      this.shared.switchPages(+id, +page.page); // pages numbers
    }
    console.log(`${type} ${id} dragged to page ${page.page}`);
  }
  toUnsorted(e) {
    let type = e.dataTransfer.getData('type');
    let id = e.dataTransfer.getData('id');
    let source_page = e.dataTransfer.getData('page');
    if (type == 'post') {
      this.shared.postToPage(id, 0); // post id, target_page, source_page
    }
    console.log(`${type} ${id} dragged to unsorted`);
  }
  onDragOver(e) {
    e.preventDefault();
  }
}
