import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageManagerComponent } from './page-manager/page-manager.component';
import { PostEditorComponent } from './post-editor/post-editor.component';
import { RouteGuardService } from './route-guard.service';
import { TagsComponent } from './tags/tags.component';

const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'page-manager', component: PageManagerComponent, canActivate: [RouteGuardService] },
  { path: 'post-editor', component: PostEditorComponent, canActivate: [RouteGuardService] },
  { path: 'tags', component: TagsComponent, canActivate: [RouteGuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
