import { Component, OnInit, HostBinding, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit, AfterViewInit {
  @Output('load') load = new EventEmitter();
  @Input('src') src;
  @Input('type') type;
  @Input('height') height;
  @ViewChild('img') img: ElementRef;
  @ViewChild('vid') vid: ElementRef;
  iframe_src;
  constructor(
    private sanitizer: DomSanitizer
  ) { }
  ngOnInit() {
    switch (this.type) {
      case 'youtube':
        this.iframe_src = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + this.src);
        break;
      case 'vimeo':
        this.iframe_src = this.sanitizer.bypassSecurityTrustResourceUrl("https://player.vimeo.com/video/" + this.src);
        break;
    }
  }
  ngAfterViewInit() {
    if (this.type == 'image' && this.height) {
      this.img.nativeElement.style.height = this.height;
      this.img.nativeElement.style.width = 'auto';
    }
    if (this.type != 'image' && this.height) {
      this.vid.nativeElement.style.width = parseInt(this.height) * 16/9 + this.height.match(/px|vh|%/gi)[0];
    }
  }
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  loaded = false;
  onLoad() {
    this.loaded = true;
    this.load.emit();
  }
}
