import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageComponent } from './image/image.component';
import { LoadingComponent } from './loading/loading.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ImageComponent,
    LoadingComponent
  ],
  exports: [
    ImageComponent,
    LoadingComponent
  ]
})
export class SharedModule { }
