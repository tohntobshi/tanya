import { Component, HostBinding } from '@angular/core';
import { loading } from '../../animations';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  animations: [ loading ]
})
export class LoadingComponent {

  @HostBinding('style.position') position = 'fixed';
  @HostBinding('style.top') top = '0px';
  @HostBinding('style.bottom') bottom = '0px';
  @HostBinding('style.left') left = '0px';
  @HostBinding('style.right') right = '0px';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.z-index') zIndex = '100500';
  @HostBinding('@loading') animation = true;

}
