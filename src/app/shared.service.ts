import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
declare var require: any;
const shuffle = require('shuffle-array');

@Injectable()
export class SharedService {


  constructor(
    private auth: AngularFireAuth,
    private db: AngularFirestore,
    private storage: AngularFireStorage
  ) {
    this.getLanguage();
    this.auth.auth.onAuthStateChanged(user => {
      if (user && user.uid === this.user) {
        this.admin = true;
      } else {
        this.admin = false;
      }
      this.user_cheked = true;
    });
    this.fetchPages();
    this.fetchTagDescs();
  }
  getLanguage() {
    let lang = localStorage.getItem('lang');
    if (['en','ru'].includes(lang)) {
      this.language = lang;
    }
  }
  setLanguage(lang: string) {
    this.language = lang;
    localStorage.setItem('lang', lang);
  }
  language = 'en';
  user: String = '72CoyIpAWVa8elBaNkV2rdsmeHf2';
  admin: boolean = false;
  user_cheked: boolean = false;
  logIn(email: string, password: string) {
    return this.auth.auth.signInWithEmailAndPassword(email, password);
  }
  logOut() {
    this.auth.auth.signOut();
  }
  pages = [];
  posts = [];
  categories = [];
  pageTypes;
  loaded = false;
  fetchPages() {
      this.db.collection('posts').ref.get().then(querySnapshot => {
        let posts = [];
        let pages = [];
        let categories = [];
        let biggestPage = 0;
        querySnapshot.forEach(p => {
          let data = p.data();
          let post = {};
          post['title_en'] = data['title_en'];
          post['content'] = data['content'];
          post['title_ru'] = data['title_ru'];
          post['short_desc_en'] = data['short_desc_en'];
          post['short_desc_ru'] = data['short_desc_ru'];
          post['order'] = data['order'];
          post['page'] = data['page'];
          post['preview'] = data['preview'];
          post['id'] = p.id;
          post['tags'] = [];

          for (var prop in data['tags']) {
            post['tags'].push(prop);
            categories.push(prop);
          }
          biggestPage = post['page'] > biggestPage ? post['page'] : biggestPage;
          posts.push(post);
        });
        this.categories = Array.from(new Set(categories)).sort((a, b) => a > b ? 1 : -1);
        this.posts = posts;
        for (let i = 0; i <= biggestPage; i++) {
          let postsOfPage = posts.filter(p => p.page == i).sort((a, b) => a.order - b.order);
          pages.push({
            page: i,
            posts: postsOfPage
          })
        }
        this.pages = pages;
        this.loaded = true;
      }).catch(e => {
        console.log(e);
      });
      this.db.collection('additional').doc('pagetypes').ref.get().then(types => {
        this.pageTypes = types.data();
      })
  }
  reloadPages() {
    this.pages = [];
    this.pageTypes = {};
    this.fetchPages();
  }
  pictures_loaded = false;
  pictures = [];
  fetchPictures() {
    if (this.pictures && this.pictures.length > 0) {
      return;
    }
    let pictures = [];
    this.db.firestore.collection('images').where('position', '>', 'backstage').where('type', '==', 'image').get().then(querySnapshot => {
      querySnapshot.forEach(e => {
        pictures.push(e.data()['src']);
      })
      this.pictures = shuffle(pictures);
      this.pictures_loaded = true;
    })
  }

  tag_descs = [];
  tagsFetched = false;
  fetchTagDescs() {
    this.db.firestore.collection('tag_descs').get().then(querySnapshot => {
      let tags = [];
      querySnapshot.forEach(r => {
        let tag = r.data();
        tag['tag'] = r.id;
        tags.push(tag);
      })
      this.tag_descs = tags;
      this.tagsFetched = true;
    })
  }
  setTag(tag) {
    this.db.firestore.collection('tag_descs').doc(tag.tag).set({
      desc_en: tag.desc_en,
      desc_ru: tag.desc_ru
    }).then(() => {
      this.fetchTagDescs();
    })
  }
  deleteTag(id) {
    this.db.firestore.collection('tag_descs').doc(id).delete().then(() => {
      this.fetchTagDescs();
    })
  }

  switchPages(a: number, b: number) {
    let tmp = this.pageTypes[a];
    this.pageTypes[a] = this.pageTypes[b];
    this.pageTypes[b] = tmp;
    let batch = this.db.firestore.batch();
    this.pages[a].posts.forEach(post => {
      let doc = this.db.firestore.collection('posts').doc(post.id);
      batch.update(doc, {
        page: +b
      })
    })
    this.pages[b].posts.forEach(post => {
      let doc = this.db.firestore.collection('posts').doc(post.id);
      batch.update(doc, {
        page: +a
      })
    })
    batch.update(this.db.firestore.doc('additional/pagetypes'), this.pageTypes);
    batch.commit().then(() => {
      this.fetchPages();
    })
  }

  postToPage(post: string, target: number) {
    let new_order = 0;
    if (target > 0) {
      new_order = this.pages[target].posts.reduce((acc, val) => val.order > acc ? val.order : acc, -1) + 1;
    }
    this.db.firestore.collection('posts').doc(post).update({
      page: +target,
      order: new_order
    }).then(() => {
      this.fetchPages();
    })
  }

  switchPosts(a: string, b: string, page: number) {
    let batch = this.db.firestore.batch();
    let post1 = this.db.firestore.collection('posts').doc(a);
    let post2 = this.db.firestore.collection('posts').doc(b);
    let order1 = this.pages[page].posts[this.pages[page].posts.findIndex(p => p.id == a)].order;
    let order2 = this.pages[page].posts[this.pages[page].posts.findIndex(p => p.id == b)].order;
    batch.update(post1, {
      order: order2
    });
    batch.update(post2, {
      order: order1
    });
    batch.commit().then(() => {
      this.fetchPages();
    })
  }
  deletePost(id) {
    this.db.firestore.collection('images').where('post', '==', id).get().then(querySnapshot => {
      let batch = this.db.firestore.batch();
      querySnapshot.forEach(i => {
        batch.delete(i.ref);
      })
      batch.delete(this.db.firestore.doc(`posts/${id}`));
      batch.commit().then(() => {
        this.fetchPages();
      })
    })

  }
  newPage() {
    let number = this.pages.reduce((biggest, current) => biggest > current.page ? biggest : current.page, 0) + 1;
    this.pages.push({
      posts: [],
      page: number
    })
    this.pageTypes[number] = 2;
    this.updatePageTypes();
  }
  updatePageTypes() {
    this.db.firestore.collection('additional').doc('pagetypes').update(this.pageTypes);
  }

  fetchPost(id: string) {
    return Promise.all([
      new Promise (resolve => {
        this.db.firestore.collection('posts').doc(id).get().then(r => {
          let data = r.data();
          let post = {};
          post['title_en'] = data['title_en'];
          post['title_ru'] = data['title_ru'];
          post['content'] = data['content'];
          post['short_desc_en'] = data['short_desc_en'];
          post['short_desc_ru'] = data['short_desc_ru'];
          post['desc_en'] = data['desc_en'];
          post['desc_ru'] = data['desc_ru'];
          post['preview'] = data['preview'];
          post['tags'] = [];
          for (var prop in data['tags']) {
            post['tags'].push(prop);
          }
          resolve(post);
        })
      }),
      new Promise (resolve => {
        this.db.firestore.collection('images').where('post', '==', id).get().then(querySnapshot => {
          let images = [];
          querySnapshot.forEach(i => {
            let data = i.data();
            images.push({
              desc_en: data['desc_en'],
              desc_ru: data['desc_ru'],
              src: data['src'],
              type: data['type'],
              position: data['position'],
              order: data['order']
            })
          });
          resolve(images.sort((a, b) => a.order - b.order));
        })
      })
    ])
  }

  uploadPicture(file) {
    let ext;
    switch (file.type) {
      case 'image/gif':
        ext = 'gif';
        break;
      case 'image/jpeg':
        ext = 'jpg';
        break;
      default:
        return Promise.resolve('');
    }
    return new Promise(resolve => {
      this.storage.storage.ref().child('images/' + this.randomString(10) + "." + ext).put(file).then(snapshot => {
        resolve(snapshot.downloadURL);
      })
    })
  }

  randomString(length: number) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  submitPost(id, post, images, new_post_id) {
    return new Promise(resolve => {
      let batch = this.db.firestore.batch();
      if (id) {
        this.db.firestore.collection("images").where('post', '==', id).get().then((querySnapshot) => {
          querySnapshot.forEach(doc => batch.delete(doc.ref));
          batch.update(this.db.firestore.collection("posts").doc(id), post);
          images.forEach(i => {
            let image = i;
            image['post'] = id;
            batch.set(this.db.firestore.collection("images").doc(this.randomString(10)), image);
          })
          batch.commit().then(()=> {
            resolve();
          })
        })

      } else if (new_post_id) {
        let newpost = post;
        newpost['order'] = 0;
        newpost['page'] = 0;
        batch.set(this.db.firestore.collection('posts').doc(new_post_id), newpost)
        images.forEach(i => {
          let image = i;
          image['post'] = new_post_id;
          batch.set(this.db.firestore.collection("images").doc(this.randomString(10)), image);
        })
        batch.commit().then(()=> {
          resolve();
        })
      } else {
        let newpost = post;
        newpost['order'] = 0;
        newpost['page'] = 0;
        this.db.firestore.collection('posts').add(newpost).then(p => {
          // let promises = [];
          images.forEach(i => {
            let image = i;
            image['post'] = p.id;
            batch.set(this.db.firestore.collection("images").doc(this.randomString(10)), image);
            // promises.push(this.db.firestore.collection("images").add(image));
          })
          // Promise.all(promises).then(() => {
          //   resolve();
          // });
          batch.commit().then(()=> {
            resolve();
          })
        })
      }

    });
  }
}
