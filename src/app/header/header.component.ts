import { Component, HostListener } from '@angular/core';
import { SharedService } from '../shared.service';
import { home } from '../animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [home]
})
export class HeaderComponent {

  constructor(
    public shared: SharedService
  ) { }

  logout() {
    this.shared.logOut();
  }
  navOpen: boolean = false;
  toggleNav() {
    this.navOpen = !this.navOpen;
  }

  moving = false;
  start;
  moveStart(pos) {
    this.moving = true;
    this.start = pos;
  }
  moveEnd(pos) {
    this.moving = false;
    if (this.start > pos + 100) {
      this.navOpen = false;
      this.start = undefined;
    }
  }
  touchEnd() {
    this.moving = false;
    this.start = undefined;
  }
  @HostListener('window: touchmove', ['$event'])
  onTouchMove(e) {
    if(!this.moving) {
      return;
    }
    if(this.start > e.touches[0].clientX + 100) {
      this.start = undefined;
      this.navOpen = false;
      this.moving = false;
    }
  }
}
