import { Component, OnInit, ViewChild, ElementRef, HostBinding } from '@angular/core';
import { SharedService } from '../shared.service';
import { home } from '../animations';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
  animations: [ home ]
})
export class FeedComponent implements OnInit {

  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@home') animation = true;
  constructor(
    public shared: SharedService,
    private title: Title
  ) { }
  ngOnInit() {
    this.title.setTitle("Tatiana Khmelevskaya")
  }


}
