import { Component, Input} from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-page-two',
  templateUrl: './page-two.component.html',
  styleUrls: ['./page-two.component.scss']
})
export class PageTwoComponent {

  @Input('columns') columns: number = 2;
  @Input('page') page;
  constructor(
    public shared: SharedService
  ) { }

}
