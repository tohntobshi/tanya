import { Component, HostListener, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements AfterViewInit {

  @ViewChild('cover') cover: ElementRef;
  @HostListener('window: scroll')
  onScroll() {
    if (window.innerWidth < 768) {
      this.cover.nativeElement.style.transform = `translateY(0px)`;
      return;
    }
    if (window.scrollY > window.innerHeight) {
      return;
    }
    this.cover.nativeElement.style.transform = `translateY(${window.scrollY/2}px)`;
  }
  ngAfterViewInit() {
    this.onScroll()
  }
}
