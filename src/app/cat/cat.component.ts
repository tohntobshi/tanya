import { Component, OnInit, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from '../shared.service';
import { home } from '../animations';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.scss'],
  animations: [ home ]
})
export class CatComponent implements OnInit {
  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@home') animation = true;
  constructor(
    public shared: SharedService,
    public route: ActivatedRoute,
    private title: Title
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(e => {
      this.category = e.get('cat');
      this.title.setTitle(this.category);
    })
  }
  category: string;
  get desc_en() {
    let index = this.shared.tag_descs.findIndex(t => t.tag === this.category);
    if (index === -1) {
      return false;
    } else {
      return this.shared.tag_descs[index].desc_en;
    }
  }
  get desc_ru() {
    let index = this.shared.tag_descs.findIndex(t => t.tag === this.category);
    if (index === -1) {
      return false;
    } else {
      return this.shared.tag_descs[index].desc_ru;
    }
  }
}
