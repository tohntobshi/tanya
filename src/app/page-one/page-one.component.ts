import { Component, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.scss']
})
export class PageOneComponent {

  @Input('page') page;
  constructor(
    public shared: SharedService
  ) { }

  @ViewChild('mainpost') mainpost: ElementRef;
  @ViewChild('maincontainer') maincontainer: ElementRef;
  @ViewChild('wholepage') wholepage: ElementRef;

  @HostListener('window: resize')
  onResize() {
    this.onScroll();
  }
  @HostListener('window: scroll')
  onScroll() {
    if(window.innerWidth < 950) {
      this.mainpost.nativeElement.style.position = 'static';
      this.mainpost.nativeElement.style.width = '100%';
      return;
    }
    this.wholepage.nativeElement.style.minHeight = this.mainpost.nativeElement.clientHeight + 'px';
    let containerposition = this.maincontainer.nativeElement.getBoundingClientRect();
    if (containerposition.top > 20) {
      this.mainpost.nativeElement.style.position = 'absolute';
      this.mainpost.nativeElement.style.top = '0px';
      this.mainpost.nativeElement.style.left = '0px';
      this.mainpost.nativeElement.style.width = '100%';
      return;
    }
    if (-containerposition.top + 20 > this.maincontainer.nativeElement.clientHeight - this.mainpost.nativeElement.clientHeight) {
      this.mainpost.nativeElement.style.position = 'absolute';
      this.mainpost.nativeElement.style.top = this.maincontainer.nativeElement.clientHeight - this.mainpost.nativeElement.clientHeight + 'px';
      this.mainpost.nativeElement.style.left = '0px';
      this.mainpost.nativeElement.style.width = '100%';
      return;
    }
    this.mainpost.nativeElement.style.position = 'fixed';
    this.mainpost.nativeElement.style.top = '20px';
    this.mainpost.nativeElement.style.left = containerposition.left + 'px';
    this.mainpost.nativeElement.style.width = this.maincontainer.nativeElement.clientWidth + 'px';
  }

}
