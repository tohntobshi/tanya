import { Component, OnInit, HostBinding } from '@angular/core';
import { home } from '../animations';
import { Title } from '@angular/platform-browser';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
  animations: [ home ]
})
export class ContactsComponent implements OnInit {
  @HostBinding('style.position') position = 'absolute';
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.width') width = '100%';
  @HostBinding('@home') animation = true;
  constructor(
    private title: Title,
    private shared: SharedService
  ) { }

  ngOnInit() {
    this.title.setTitle(this.shared.language == 'en' ? "Contacts" : "Контакты")
  }

}
