import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';

import { SharedService } from './shared.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FeedComponent } from './feed/feed.component';
import { PageOneComponent } from './page-one/page-one.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactsComponent } from './contacts/contacts.component';
import { CategoriesComponent } from './categories/categories.component';
import { PostComponent } from './post/post.component';
import { PageTwoComponent } from './page-two/page-two.component';
import { PageThreeComponent } from './page-three/page-three.component';
import { CatComponent } from './cat/cat.component';
import { PostPreviewComponent } from './post-preview/post-preview.component';
import { CoverComponent } from './cover/cover.component';
import { SocialLinksComponent } from './social-links/social-links.component';

const firebaseConf = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBOUeapvR4Uklzg5YgzG_tWAmOmWN73OJ8",
    authDomain: "tanya-d5cd3.firebaseapp.com",
    databaseURL: "https://tanya-d5cd3.firebaseio.com",
    projectId: "tanya-d5cd3",
    storageBucket: "tanya-d5cd3.appspot.com",
    messagingSenderId: "593175759063"
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FeedComponent,
    PageOneComponent,
    GalleryComponent,
    ContactsComponent,
    CategoriesComponent,
    PostComponent,
    PageTwoComponent,
    PageThreeComponent,
    CatComponent,
    PostPreviewComponent,
    CoverComponent,
    SocialLinksComponent

  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(firebaseConf.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    SharedModule
  ],
  providers: [
    SharedService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
