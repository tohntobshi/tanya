import { animate, AnimationEntryMetadata, state, style, transition, trigger, group } from '@angular/core';

export const home: AnimationEntryMetadata =
  trigger('home', [
      state('*', style({
        opacity: '1'
      })),
      transition('void => *', [
        style({
          opacity: '0',
        }),
        animate('0.5s ease-out')
        ]),
        transition('* => void', animate('0.5s ease-in', style({
          opacity: '0',
      })))
  ]);
export const loading: AnimationEntryMetadata =
  trigger('loading', [
    state('*', style({
      opacity: '1'
    })),
    transition('void => *', [
      style({
        opacity: '0',
      }),
      animate('0.5s ease-out')
    ]),
    transition('* => void', animate('0.5s 1s ease-in', style({
      opacity: '0',
    })))
  ]);
export const post: AnimationEntryMetadata =
  trigger('post', [
    state('*', style({
      opacity: '1'
    })),
    transition('void => *', [
      style({
        opacity: '0',
      }),
      animate('0.5s ease-out')
    ])
  ]);
